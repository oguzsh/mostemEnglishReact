import React, { Component } from "react";
import Button from "./components/Button";
import "./App.css";
import "./modal.css";

class App extends Component {
  constructor(props) {
    super(props);
    this.state = { number: null, toggleFirst: false, toggleSecond: false };

    this.toggleFirst = this.toggleFirst.bind(this);
    this.toggleSecond = this.toggleSecond.bind(this);
  }

  toggleFirst = e => {
    this.setState(prevState => ({ toggleFirst: !prevState.toggleFirst }));
  };

  toggleSecond = event => {
    this.setState(prevState => ({ toggleSecond: !prevState.toggleSecond }));
  };

  createRandomNum = _ => {
    const randomNumber = Math.floor(Math.random() * 101);
    return randomNumber;
  };

  onClick = _ => {
    const number = this.createRandomNum();
    this.setState({ number: number });
  };

  render() {
    const display = {
      display: "block"
    };
    const hide = {
      display: "none"
    };
    return (
      <div className="container">
        <div className="left-half">
          <article>
            <div className="box">
              <p>Mostem English</p>
            </div>
            <div className="buttons">
              <Button name="Nasıl Oynanır ?" onClick={this.toggleFirst} />
              <div
                className="modal"
                style={this.state.toggleFirst ? display : hide}
              >
                <div className="modal-main">
                  <button className="close" onClick={this.toggleFirst}>
                    X
                  </button>
                  <h1>Nasıl Oynanır ?</h1>
                  <p>
                    Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                    Error sapiente, saepe, consectetur, in voluptatem veniam
                    voluptatibus inventore asperiores odio dolorum ratione
                    consequuntur culpa facilis reiciendis numquam recusandae
                    laborum rerum assumenda.
                  </p>
                </div>
              </div>
              <Button name="Kurallar" onClick={this.toggleSecond} />
              <div
                className="modal"
                style={this.state.toggleSecond ? display : hide}
              >
                <div className="modal-main">
                  <button className="close" onClick={this.toggleSecond}>
                    X
                  </button>
                  <h1>Kurallar</h1>
                  <p>
                    Lorem, ipsum dolor sit amet consectetur adipisicing elit.
                    Error sapiente, saepe, consectetur, in voluptatem veniam
                    voluptatibus inventore asperiores odio dolorum ratione
                    consequuntur culpa facilis reiciendis numquam recusandae
                    laborum rerum assumenda.
                  </p>
                </div>
              </div>
            </div>
            <div className="footer">
              <p>Copyright © 2018 | Powered By</p>
              <a
                className="btn link"
                href="https://oguzsh.github.io/"
                target="_blank"
              >
                oguzsh
              </a>
            </div>
          </article>
        </div>
        <div className="right-half">
          <article>
            <h1>Hadi Başlayalım</h1>
            <div className="getNumber">
              <h4>Numaranızı Almak İçin</h4>
              <Button name="BASINIZ" onClick={this.onClick} className="blue">
                BASINIZ
              </Button>
            </div>
            <p> Numaranız : {this.state.number} </p>
          </article>
        </div>
      </div>
    );
  }
}

export default App;
